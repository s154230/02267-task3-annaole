package token.tokenManager.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeFactory;

public class TokenManager {

	// In-memory database
	private static Repository repo = new InMemoryRepository();

	public static final int BAR_HEIGHT = 60;
	public static final int BAR_WIDTH = 2;

	public static List<Token> requestTokens(String customerId, int amount) throws Exception {

		if (amount < 1 || amount > 5 ){
			throw new Exception("Can only request 1-5 tokens");
		}
		// If user is registered
		// check if they have at most one unused token left
		List<Token> newTokens = new ArrayList<Token>();

		Customer currentCustomer = repo.getCustomer(customerId);		

		if (currentCustomer != null) {
			List<Token> oldTokens = currentCustomer.getTokens();

			int unusedTokens = 0;

			for (int i = 0; i < oldTokens.size(); i++) {
				if (oldTokens.get(i) != null && !oldTokens.get(i).isUsed()) {
					unusedTokens++;
					newTokens.add(oldTokens.get(i));
				}

				if (unusedTokens > 1) {
					throw new Exception("Too many unused tokens");
				}
			}
		} else {
			currentCustomer = new Customer(customerId);
			repo.addCustomer(currentCustomer);
		}

		// Customer may request 1-5 tokens
		for (int i = 0; i < amount; i++) {
			String uniqueString = makeUniqueString();

			Barcode barcode = BarcodeFactory.createCode128B(uniqueString);
			barcode.setBarHeight(BAR_HEIGHT);
			barcode.setBarWidth(BAR_WIDTH);

			newTokens.add(new Token(uniqueString));

			// Save in DB
			//repo.addToken(newTokens.get(i));

			//File imgFile = new File(uniqueString+".png");
			// TODO: Implement a file deletion function
			// to be called when tests are finished.

			// Write bar code to PNG file
			//BarcodeImageHandler.savePNG(barcode,  imgFile);
		}

		repo.updateCustomer(currentCustomer, newTokens);

		return newTokens;
	}

	public static List<Token> useToken(String tokenId) throws Exception {
		List<Token> tokens = new ArrayList<Token>();
		
		List<Customer> customers = repo.getAllCustomers();
		for (Customer c : customers) {
			tokens = c.getTokens();
			for (Token t : tokens) {
				if (t.getString().equals(tokenId)) {
					if (t.isUsed()) {
						throw new Exception("Token has already been used");
					}
					
					t.use();
					// TODO: does updateCustomer work with c? or should it be cId?
					repo.updateCustomer(c, tokens);
					return tokens;
				}
			}
			throw new Exception("Token does not exist");
		}
		return null;
	}

	public static String makeUniqueString(){
		return UUID.randomUUID().toString().substring(0, 10);
	}

	public static Repository getRepo() {
		return repo;
	}

}
