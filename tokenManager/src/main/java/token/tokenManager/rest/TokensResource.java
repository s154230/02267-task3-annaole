package token.tokenManager.rest;


import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/tokens")
public class TokensResource {
	
	@Context
	UriInfo uriInfo;
	
	// POST, because requestToken is not idempotent
	@POST
	@Consumes("text/plain")
	@Produces("text/plain")
	public Response requestTokens(String customerId, @QueryParam("amount") int amount) {
		try {
			//List<Token> tokens = TokenManager.requestTokens(customerId ,amount);
			List<Token> tokens = new ArrayList<Token>();
			tokens.add(new Token("1111"));
			
			JSONObject tokenObj = new JSONObject();
			tokenObj.put("tokens", tokens);

			return Response.ok(tokenObj.toString()).build();
		
		} catch (Exception e){
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}	
}

