package token.tokenManager.rest;

import java.util.ArrayList;
import java.util.List;

public class InMemoryRepository implements Repository {
	List<Token> tokens = new ArrayList<Token>();
	List<Customer> customers = new ArrayList<Customer>();

	// Check if customer ID already exists in DB.
	// Used when trying to register a new customer.
	public Customer getCustomer(String id){
		for (int i =0; i<customers.size(); i++){
			if (customers.get(i).getID().equals(id)){
				return customers.get(i);
			}
		}
		//TODO Throw error
		return null;
	}

	public void clearRepository(){
		tokens.clear();
		customers.clear();
	}
	public void addCustomer(Customer customer) {
		customers.add(customer);
	}

	public void removeCustomer(Customer customer) {
		customers.remove(customer);
	}

	public void updateCustomer(Customer customer, List<Token> newTokens) {
		customers.remove(customer);
		customer.updateTokens(newTokens);
		customers.add(customer);
	}

	public List<Token> getCustomerTokens(String customerId) {
		for (int i =0; i<customers.size(); i++){
			if (customers.get(i).getID().equals(customerId)){
				return customers.get(i).getTokens();
			}
		}
		//TODO Throw error
		return null;
	}
	
	public List<Customer> getAllCustomers() {
		return customers;
	}
}
