package Task3.Task3;

public class StringCalculator {

	public static void main(String[] args) {
		System.out.println("Hello!");

	}
	
	public static int Add(String numbers) throws NegativeNumbersException {
		if (numbers.isEmpty()) {
			return 0;
		} 
		
		// Used in for loop
		int iValue = 0;
		
		// Default delimiter: ","
		char delimiter = ',';
		
		// Check if different delimiter is used
		if (numbers.contains("//")) {
			String[] numberList = numbers.split("\n");
			
			char[] charList = numberList[0].toCharArray();
			delimiter = charList[2];
			
			System.out.println("New delimiter: " + delimiter);
			
			// Ensure that for loop skips first two entries (due to ; and \n)
			iValue = 2;
		}
		
		String[] numberList = numbers.split(delimiter+"|\n");

		
		int sum = 0;
		
		// Check for negative numbers
		boolean negativesExist = false;
		String negatives = "";
		
		for (int i = iValue; i < numberList.length; i++) {
			int number = Integer.parseInt(numberList[i]);
			if (number < 0) {
				negativesExist = true;
				negatives += number;
			}
			sum += number;
		}
		
		if (negativesExist) {
			throw new NegativeNumbersException("Negatives not allowed: "+ negatives);
		}
		
		return sum;
	}

}