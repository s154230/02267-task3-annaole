package com.example.demo.rest;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;

public class DTUPayApplication {

	// In-memory database
	private static InMemoryRepository repo = new InMemoryRepository();

	public static final int BAR_HEIGHT = 60;
	public static final int BAR_WIDTH = 2;

	public static List<Token> requestTokens(String customerId, int amount) throws Exception {

		if (amount < 1 || amount > 5 ){
			throw new Exception("Can only request 1-5 tokens");
		}
		// If user is registered
		// check if they have at most one unused token left
		List<Token> newTokens = new ArrayList<Token>();

		Customer currentCustomer = repo.getCustomer(customerId);		

		if (currentCustomer != null) {
			List<Token> oldTokens = currentCustomer.getTokens();

			int unusedTokens = 0;

			for (int i = 0; i < oldTokens.size(); i++) {
				if (oldTokens.get(i) != null && !oldTokens.get(i).isUsed()) {
					unusedTokens++;
					newTokens.add(oldTokens.get(i));
				}

				if (unusedTokens > 1) {
					throw new Exception("Too many unused tokens");
				}
			}
		} else {
			currentCustomer = new Customer(customerId);
			repo.addCustomer(currentCustomer);
		}

		// Customer may request 1-5 tokens
		for (int i = 0; i < amount; i++) {
			String uniqueString = makeUniqueString();

			Barcode barcode = BarcodeFactory.createCode128B(uniqueString);
			barcode.setBarHeight(BAR_HEIGHT);
			barcode.setBarWidth(BAR_WIDTH);

			newTokens.add(new Token(uniqueString));

			// Save in DB
			//repo.addToken(newTokens.get(i));

			//File imgFile = new File(uniqueString+".png");
			// TODO: Implement a file deletion function
			// to be called when tests are finished.

			// Write bar code to PNG file
			//BarcodeImageHandler.savePNG(barcode,  imgFile);
		}

		repo.updateCustomer(currentCustomer, newTokens);

		return newTokens;
	}

	public static List<Token> useToken(String customerId, String tokenid) throws Exception {
		List<Token> tokens = new ArrayList<Token>();
		// Ensure token is valid

		Customer currentCustomer = repo.getCustomer(customerId);
		if (currentCustomer != null) {
			tokens = currentCustomer.getTokens();

			for (int i = 0; i < tokens.size(); i++) {
				if (tokens.get(i) != null && tokens.get(i).getString().equals(tokenid)) {
					if (!tokens.get(i).isUsed()) {

						// Update token in DB
						tokens.get(i).use();

						repo.updateCustomer(currentCustomer, tokens);

						return tokens;

					} else {
						throw new Exception("Token has already been used");
					}
				}
			}
			throw new Exception("Token does not exist");
		}
		throw new Exception("Unauthorized use of token");
	}
	
	public static String makeUniqueString(){
		return UUID.randomUUID().toString().substring(0, 10);
	}

	public static InMemoryRepository getRepo() {
		return repo;
	}

}
